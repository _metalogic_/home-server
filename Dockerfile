FROM golang:1.13 as builder

WORKDIR /build

COPY ./ /build

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o command .

FROM metalogic/alpine:latest

RUN adduser -u 25010 -g 'Application Runner' -D runner

WORKDIR /home/runner

COPY --from=builder /build/command home-server

RUN mkdir -p /data/htdocs && chown runner /data/htdocs

EXPOSE 8080

USER runner

# start the server with document root /data/htdocs
CMD ./home-server -d /data/htdocs

