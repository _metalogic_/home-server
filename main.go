package main

import (
	"flag"
	"log"
	"net/http"
	"os"
)

var (
	dirFlg  string
	homeDoc string
)

func main() {

	flag.StringVar(&dirFlg, "d", "./htdocs", "root directory for the web server")

	flag.Parse()

	homeDoc = dirFlg + "/index.html"

	if _, err := os.Stat(homeDoc); os.IsNotExist(err) {
		log.Fatalf("%s\n", err)
	}

	http.HandleFunc("/", serveHome)
	http.ListenAndServe(":8080", nil)
}

func serveHome(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.Error(w, "Not found", http.StatusNotFound)
		return
	}
	if r.Method != "GET" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}
	http.ServeFile(w, r, homeDoc)
}
