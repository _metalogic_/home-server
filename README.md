# home-server

## Build and Run in Go

Create a file htdocs/index.html. Enter your message in index.html.

``` $ go build ```

``` $ ./home-server ```

You can provide a different document root directory with the -d option:

``` $ ./home-server -d /tmp/some-other-root```

## Building the Docker Image

``` $ docker build -t home-server .```


